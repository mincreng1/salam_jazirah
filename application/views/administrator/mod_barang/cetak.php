<!DOCTYPE html>
<html>
    <head>
        <title>Cetak Laporan Supplier</title>

        <style>
        #watermark { position: fixed; bottom: 0px; right: 0px; width: 500px; height: 450px; opacity: .1; }
        @page { margin-top: 30px; }
        img{ text-align: right; } table {
        border-collapse: collapse;
        }
        body {
        font-family: "Arial";
        font-size:9;
        }
        .header, .footer {
        width: 100%;
        text-align: right;
        position: fixed;
        }
        .footer2 {
        width: 100%;
        text-align: left;
        position: fixed;
        }
        .header {
        top: 0px;
        }
        .footer {
        bottom: 0px;
        }
        .footer2 {
        bottom: 0px;
        }
        .pagenum:before {
        content: counter(page);
        }
        table, td, th {
        border: 1px solid black;
        padding: 10px;
        }
        table {
        border-collapse: collapse;
        width: 100%;
        }
        th {
        height: 50px;
        }
        </style>

    </head>

    <body onload="window.print()">
        <?php
        $html ='
        <center>
        <h1>Laporan Supplier</h1>
        <table border="1" align="center" width="100%">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th>Nama</th>
                    <th>Kontak</th>
                    <th>Alamat</th>
                    <th>No HP</th>
                </tr>
            </thead>
            <tbody>';
                $i=1;
                foreach ($record as $lihat) {
                $html .= '<tr>
                    <td><center>'. $i . '.</center></td>
                    <td>'. ucfirst($lihat[nama_supplier]) .'</td>
                    <td>'. ucfirst($lihat[kontak_person]) .'</td>
                    <td>'. ucfirst($lihat[alamat_lengkap]) .'</td>
                    <td>'. ucfirst($lihat[no_hp]) .'</td>
                    
                </tr>';
                $i++;
                }
            $html .= '</tbody>
        </table>
        </center>';
        echo $html;
        ?>
    </body>
    
</html>