<!DOCTYPE html>
<html>
    <head>
        <title>Cetak Laporan Pendapatan </title>

        <style>
        #watermark { position: fixed; bottom: 0px; right: 0px; width: 500px; height: 450px; opacity: .1; }
        @page { margin-top: 30px; }
        img{ text-align: right; } table {
        border-collapse: collapse;
        }
        body {
        font-family: "Arial";
        font-size:9;
        }
        .header, .footer {
        width: 100%;
        text-align: right;
        position: fixed;
        }
        .footer2 {
        width: 100%;
        text-align: left;
        position: fixed;
        }
        .header {
        top: 0px;
        }
        .footer {
        bottom: 0px;
        }
        .footer2 {
        bottom: 0px;
        }
        .pagenum:before {
        content: counter(page);
        }
        table, td, th {
        border: 1px solid black;
        padding: 10px;
        }
        table {
        border-collapse: collapse;
        width: 100%;
        }
        th {
        height: 50px;
        }
        </style>

    </head>

    <body onload="window.print()">
        <?php
        $html ='
        <center>
        <h1>Laporan Pendapatan</h1>
        <h5>Periode '. tgl_indo($mulai) .' - ' . tgl_indo($sampai) . '</h5>
        <table border="1" align="center" width="100%">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th><center>Total Pembelian Barang</center></th>
                    <th><center>Total Penjualan Barang</center></th>
                    <th><center>Keuntungan</center></th>
                </tr>
            </thead>
            <tbody>';

                $penjualan = $this->db->query("SELECT * FROM rb_penjualan, rb_penjualan_detail WHERE rb_penjualan.id_penjualan = rb_penjualan_detail.id_penjualan AND rb_penjualan.waktu_transaksi BETWEEN '". $mulai ."' AND '". $sampai ."'")->result();

                $pembelian = $this->db->query("SELECT * FROM rb_pembelian, rb_pembelian_detail WHERE rb_pembelian.id_pembelian = rb_pembelian_detail.id_pembelian AND rb_pembelian.waktu_beli BETWEEN '". $mulai ."' AND '". $sampai ."'")->result();

                foreach ($penjualan as $row) {
                   $total_penjualan += $row->jumlah * $row->harga_jual;
                }

                foreach ($pembelian as $row) {
                   $total_pembelian += $row->jumlah_pesan * $row->harga_pesan;
                }

                $keuntungan = $total_penjualan - $total_pembelian;

                $html .= '<tr>
                        <td><center>1</center></td>
                        <td><center>'. rupiah($total_pembelian) . '</center></td>
                        <td><center>'. rupiah($total_penjualan) . '</center></td>
                        <td><center>'. rupiah($keuntungan) . '</center></td>
                    
                        </tr>';
            $html .= '</tbody>
        </table>
        </center>';
        echo $html;
        ?>
    </body>
    
</html>