            <div class="col-xs-12">  
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Laporan Pendapatan </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <form method="POST" action="<?= base_url('administrator/laporanpendapatan'); ?>" class="form-vertical">
                    <div class="col-md-3">
                      <label class="">Mulai Tanggal</label>
                    </div>
                    <div class="col-md-9">
                      <input type="text" name="mulai" class="form-control"  value="<?= date('Y-m-d'); ?>">
                    </div>
                    <br/><br/>
                    <div class="col-md-3">
                      <label class="">Sampai Tanggal</label>
                    </div>
                    <div class="col-md-9">
                      <input type="text" name="sampai" class="form-control" value="<?= date('Y-m-d'); ?>">
                    </div>
                    <br/><br/><br/>

                    <button class="btn btn-primary pull-right" type="submit" name="submit">
                      Cari
                    </button>
                  </form>
                </div>