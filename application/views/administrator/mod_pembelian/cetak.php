<!DOCTYPE html>
<html>
    <head>
        <title>Cetak Laporan Pembelian</title>

        <style>
        #watermark { position: fixed; bottom: 0px; right: 0px; width: 500px; height: 450px; opacity: .1; }
        @page { margin-top: 30px; }
        img{ text-align: right; } table {
        border-collapse: collapse;
        }
        body {
        font-family: "Arial";
        font-size:9;
        }
        .header, .footer {
        width: 100%;
        text-align: right;
        position: fixed;
        }
        .footer2 {
        width: 100%;
        text-align: left;
        position: fixed;
        }
        .header {
        top: 0px;
        }
        .footer {
        bottom: 0px;
        }
        .footer2 {
        bottom: 0px;
        }
        .pagenum:before {
        content: counter(page);
        }
        table, td, th {
        border: 1px solid black;
        padding: 10px;
        }
        table {
        border-collapse: collapse;
        width: 100%;
        }
        th {
        height: 50px;
        }
        </style>

    </head>

    <body onload="window.print()">
        <?php
        $html ='
        <center>
        <h1>Laporan Pembelian</h1>
        <table border="1" align="center" width="100%">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th>Kode Pembelian</th>
                    <th>Nama Supplier</th>
                    <th>Waktu Pembelian</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>';
                $i=1;
                foreach ($record as $row) {
                    $total = $this->db->query("SELECT sum(a.harga_pesan*a.jumlah_pesan) as total FROM `rb_pembelian_detail` a where a.id_pembelian='$row[id_pembelian]'")->row_array();
                    $total_pembelian += $total['total'];
                $html .= '<tr>
                    <td><center>'. $i . '.</center></td>
                    <td>'. ucfirst($row[kode_pembelian]) .'</td>
                    <td>'. ucfirst($row[nama_supplier]) .'</td>
                    <td>'. ucfirst($row[waktu_beli]) .'</td>
                    <td>Rp '.rupiah($total['total']).'</td>
                    
                </tr>';
                $i++;
                }
            $html .= '
                    <tr>
                        <td colspan="4" align="right"><b>Total Pembelian Barang</b></td>
                        <td>Rp. '. rupiah($total_pembelian) .'</td>
                    </tr>
            </tbody>
        </table>
        </center>';
        echo $html;
        ?>
    </body>
    
</html>