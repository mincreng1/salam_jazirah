<!DOCTYPE html>
<html>
    <head>
        <title>Cetak Laporan Produk</title>

        <style>
        #watermark { position: fixed; bottom: 0px; right: 0px; width: 500px; height: 450px; opacity: .1; }
        @page { margin-top: 30px; }
        img{ text-align: right; } table {
        border-collapse: collapse;
        }
        body {
        font-family: "Arial";
        font-size:9;
        }
        .header, .footer {
        width: 100%;
        text-align: right;
        position: fixed;
        }
        .footer2 {
        width: 100%;
        text-align: left;
        position: fixed;
        }
        .header {
        top: 0px;
        }
        .footer {
        bottom: 0px;
        }
        .footer2 {
        bottom: 0px;
        }
        .pagenum:before {
        content: counter(page);
        }
        table, td, th {
        border: 1px solid black;
        padding: 10px;
        }
        table {
        border-collapse: collapse;
        width: 100%;
        }
        th {
        height: 50px;
        }
        </style>

    </head>

    <body onload="window.print()">
        <?php
        $html ='
        <center>
        <h1>Laporan Produk</h1>
        <table border="1" align="center" width="100%">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th>Nama Produk</th>
                    <th>Harga Modal</th>
                    <th>Harga Reseller</th>
                    <th>Harga Konsumen</th>
                    <th>Stok</th>
                    <th>Satuan</th>
                    <th>Berat</th>
                </tr>
            </thead>
            <tbody>';
                $i=1;
                foreach ($record as $row) {
                    $jual = $this->model_app->jual($row['id_produk'])->row_array();
                    $beli = $this->model_app->beli($row['id_produk'])->row_array();
                $html .= '<tr>
                  <td><center>'. $i . '.</center></td>
                  <td>' . $row[nama_produk] .'</td>
                  <td>Rp '.rupiah($row['harga_beli']).'</td>
                  <td>Rp '.rupiah($row['harga_reseller']).'</td>
                  <td>Rp '.rupiah($row['harga_konsumen']).'</td>
                  <td>'.($beli['beli']-$jual['jual']).'</td>
                  <td>'. $row[satuan] .'</td>
                  <td>'. $row[berat] .'Gram</td>
                    
                </tr>';
                $i++;
                }
            $html .= '</tbody>
        </table>
        </center>';
        echo $html;
        ?>
    </body>
    
</html>