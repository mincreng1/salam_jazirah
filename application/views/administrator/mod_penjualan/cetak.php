<!DOCTYPE html>
<html>
    <head>
        <title>Cetak Laporan Penjualan</title>

        <style>
        #watermark { position: fixed; bottom: 0px; right: 0px; width: 500px; height: 450px; opacity: .1; }
        @page { margin-top: 30px; }
        img{ text-align: right; } table {
        border-collapse: collapse;
        }
        body {
        font-family: "Arial";
        font-size:9;
        }
        .header, .footer {
        width: 100%;
        text-align: right;
        position: fixed;
        }
        .footer2 {
        width: 100%;
        text-align: left;
        position: fixed;
        }
        .header {
        top: 0px;
        }
        .footer {
        bottom: 0px;
        }
        .footer2 {
        bottom: 0px;
        }
        .pagenum:before {
        content: counter(page);
        }
        table, td, th {
        border: 1px solid black;
        padding: 10px;
        }
        table {
        border-collapse: collapse;
        width: 100%;
        }
        th {
        height: 50px;
        }
        </style>

    </head>

    <body onload="window.print()">
        <?php
        $html ='
        <center>
        <h1>Laporan Penjualan</h1>
        <table border="1" align="center" width="100%">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th>Kode Transaksi</th>
                    <th>Pengiriman</th>
                    <th>Tujuan</th>
                    <th>Waktu Transaksi</th>
                    <th>Total Belanja</th>
                </tr>
            </thead>
            <tbody>';
                $i=1;
                foreach ($record->result_array() as $row){
                    if ($row['proses']=='0'){ $proses = '<i class="text-danger">Pending</i>'; $color = 'danger'; $text = 'Pending'; }elseif($row['proses']=='1'){ $proses = '<i class="text-warning">Proses</i>'; $color = 'warning'; $text = 'Proses'; }elseif($row['proses']=='2'){ $proses = '<i class="text-info">Konfirmasi</i>'; $color = 'info'; $text = 'Konfirmasi'; }else{ $proses = '<i class="text-success">Packing </i>'; $color = 'success'; $text = 'Packing'; }
                   $total = $this->db->query("SELECT a.kode_transaksi, a.kurir, a.service, a.proses, a.ongkir, e.nama_kota, f.nama_provinsi, sum((b.harga_jual*b.jumlah)-(c.diskon*b.jumlah)) as total, sum(c.berat*b.jumlah) as total_berat FROM `rb_penjualan` a JOIN rb_penjualan_detail b ON a.id_penjualan=b.id_penjualan JOIN rb_produk c ON b.id_produk=c.id_produk JOIN rb_konsumen d ON a.id_pembeli=d.id_konsumen JOIN rb_kota e ON d.kota_id=e.kota_id JOIN rb_provinsi f ON e.provinsi_id=f.provinsi_id where a.kode_transaksi='$row[kode_transaksi]'")->row_array();
                   $total_penjualan += $total['total']+$total['ongkir'];
                $html .= '<tr>
                    <td><center>'. $i . '.</center></td>
                    <td>'. ucfirst($row[kode_transaksi]) .'</td>
                    <td><span style="text-transform:uppercase">'. $total[kurir] . '</span>'. ($total[service]). '</td>
                    <td>'. $total[nama_provinsi] . '-> <span class="text-danger">' . $total[nama_kota] . '</span></td>
                    <td>'.$row[waktu_transaksi].'</td>
                     <td style="color:red;">Rp '.rupiah($total['total']+$total['ongkir']).'</td>
                    
                </tr>';
                $i++;
                }
            $html .= ' 
            <tr>
                <td align="right">Total Penjualan</td>
                <td colspan="5" > Rp. '. rupiah($total_penjualan) . ' (' . terbilang($total_penjualan) . ' Rupiah)</td>
            </tr>
            </tbody>
        </table>
        </center>';
        echo $html;
        ?>
    </body>
    
</html>